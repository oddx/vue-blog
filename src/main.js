import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import Edit from './Edit.vue'
import App from './App.vue'
import Blog from './Blog.vue'
import Login from './Login.vue'
import Post from './Post.vue'
import config from './config.js'

Vue.use(VueResource);
Vue.use(VueRouter);

const auth = (from, to, next) => {
   if (document.cookie && from.path !== '/login') {
      fetch(config.serverUrl + 'auth', { credentials: 'include' })
         .then(data => {
            if (data.status != 200) next('/login')
            else return next()
      }).catch(console.dir)
   } else if (from.path === '/login') {
     return next()
   } else {
     return next('/login')
   }   
}

const router = new VueRouter({
   mode: 'history',
   routes: [
      {
         path: '/',
         component: Blog
      },
      {
         path: '/login',
         component: Login,
         beforeEnter: auth
      },
      {
         path: '/post/:id',
         component: Post,
         props: true
      },
      {
         path: '/edit/:id',
         component: Edit,
         props: true,
         beforeEnter: auth
      },
      {
         path: '/edit',
         component: Edit,
         beforeEnter: auth
      }
   ]
})

new Vue({
   el: '#app',
   router,
   render: h => h(App)
})
