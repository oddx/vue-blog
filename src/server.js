import gm from 'gm'
import multer from 'multer'
import cookieParser from 'cookie-parser'
import compression from 'compression'
import express from 'express'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import fs from 'fs'
import MongoClient from 'mongodb'
import showdown from 'showdown'
import config from './config.js'

const app = express()
const upload = multer({storage: multer.memoryStorage()})
const allowCrossDomain = function (req, res, next) {
   res.header('Access-Control-Allow-Origin', config.clientUrl)
   res.header('Access-Control-Allow-Methods', 'GET,OPTIONS,PUT,POST,DELETE')
   res.header('Access-Control-Allow-Headers', 'Content-Type')
   res.header('Access-Control-Allow-Credentials', true)
   return next()
}

app.use(allowCrossDomain,
   express.json(),
   cookieParser(),
   compression()
)

let converter = new showdown.Converter()

let db
MongoClient.connect('mongodb://' + config.mongoUser + ':' + config.mongoPass + '@localhost:27017?authSource=test', { useUnifiedTopology: true })
   .then(client => db = client.db(config.mongoSchema).collection('travelsPosts'))
   .catch(err => console.dir(err))

app.get('/api/greet', (req, res) => {
   console.dir('greetings!')
   res.status(200).send({message: 'heyo'})
})

app.get('/api/auth/', async (req, res) => {
   //verify admin with cookie
   if (req.cookies && req.cookies.isAdmin) {
      try { await jwt.verify(req.cookies.isAdmin, config.jwtSecret) }
      catch (e) { res.status(401).send(e) }
      res.status(200).send()
   } else res.status(401).send()
})

app.get('/api/posts/', async (req, res) => {
   const docs = await db.find()
                        .skip(req.query.page * 10)
                        .limit(10)
                        .sort({_id: -1})
                        .map(doc => {
                           return {
                              _id: doc._id,
                             title: doc.title
                                    }})
                        .toArray()
   console.log(docs)
   res.status(200).send(docs)
})

app.get('/api/posts/count', async (req, res) => {
   res.status(200).send({'count': await db.countDocuments()})
})

app.get('/api/post/:id', async (req, res) => {
   const doc = await db.find({'_id': MongoClient.ObjectId(req.params.id)})
                        .toArray()
   console.log(doc)
   res.status(200).send(doc)
})

app.get('/api/media/:media', (req, res) => {
   const media = fs.readFileSync(
      'media/' + req.params.media
   )
   console.log('found ' + req.params.media)
   res.status(200).send(media)
})

app.post('/api/login/', async (req, res) => {
   //receive credentials, compare hashes, send cookie token
   if (req.body.user === config.adminUser &&
      await bcrypt.compare(req.body.pass, config.adminPass)) {
         
      const token = jwt.sign(
         {user: req.body.user}, config.jwtSecret, {expiresIn: '30 minutes'}
      )
      console.dir('logged in ' + req.body.user)
      res.cookie('isAdmin', token, {maxAge: 3600000, httpOnly: false}).status(200).send()
   } else res.status(401).send()
})

app.post('/api/posts/', upload.array('photos'), (req, res) => {
   const doc = {
      title: req.body.title,
      content: converter.makeHtml(req.body.content),
      media: [],
      timestamp : new Date()
   }
   db.insertOne(doc)
      
   if (req.files) {
      if (!fs.existsSync(config.mediaPath)) { fs.mkdirSync(config.mediaPath) }

      req.files.forEach((file, id) => {
         const filename = doc._id.toString() + '-' + id + '.jpg'
         const uri = config.mediaPath + filename
         fs.writeFile(uri, file.buffer,
         err => {
            if (err) res.status(400).send(err)
            else {
               gm(uri)
                  .resize(1600, 1600)
                  .write(uri,
                  err => { if (err) res.status(400).send(err)})
               db.findOneAndUpdate(
                  {'_id': MongoClient.ObjectId(doc._id)},
                  {$push: {'media': filename}}
               )
               console.dir('added ' + filename)
            }
         })
      })
   console.dir('posted blog: ' + JSON.stringify(doc))
   res.status(200).send()
   } else res.status(200).send()
})

app.post('/api/delete/', (req, res) => {
   //drop post from collection,
   req.body._id.forEach(async post => {
      try {
         const doc = await db.findOne({'_id': MongoClient.ObjectId(post)})
         if (doc) {
            if (doc.media){ doc.media.forEach(file => fs.unlinkSync(config.mediaPath + file)) }
            db.deleteOne({'_id': MongoClient.ObjectId(post)})
            console.dir('deleted ' + doc._id + ': ' + doc.title)
         } else { console.dir('doc ' + post + ' does not exist') }
         res.status(200).send()
      } catch (err) { res.status(400).send(err) }
   })
})

app.post('/api/edit/:_id', upload.array('photos'), (req, res) => {
   const doc = {
      _id: req.params._id,
      title: req.body.title,
      content: converter.makeHtml(req.body.content),
      media:  typeof req.body.media == 'string' ?
               [req.body.media] :
               req.body.media || [],
      updateTimestamp : new Date()
   }
   db.updateOne({'_id': MongoClient.ObjectId(doc._id)},
               {$set: {
                  'title': doc.title,
                  'content': doc.content,
                  'updateTimestamp': doc.updateTimestamp
               }})

   const deleteFiles = fs.readdirSync(config.mediaPath)
                        .filter(filename => !doc.media.includes(filename))
                        .filter(media => media.includes(doc._id))
               
   if (deleteFiles) {
      deleteFiles.forEach(media => {
         fs.unlinkSync(config.mediaPath + media)
         db.findOneAndUpdate(
            {'_id': MongoClient.ObjectId(doc._id)},
            {$pull: {'media': media}})
      }) 
   }
    
   if (req.files) {
      if (!fs.existsSync(config.mediaPath)) { fs.mkdirSync(config.mediaPath) }

      const filesSize = fs.readdirSync(config.mediaPath) 
                        .filter(media => media.includes(doc._id))
                        .length
                        
      req.files.forEach((file, id) => {
         const index = filesSize + id 
         const filename = doc._id.toString() + '-' + index + '.jpg'
         const uri = config.mediaPath + filename
         if (!fs.existsSync(uri)) {
            fs.writeFile(uri, file.buffer,
            err => {
               if (err) res.status(400).send(err)
               else {
                  gm(uri)
                     .resize(1600, 1600)
                     .write(uri,
                     err => { if (err) res.status(400).send(err)})
                  db.findOneAndUpdate(
                     {'_id': MongoClient.ObjectId(doc._id)},
                     {$push: {'media': filename}}
                  )
                  console.dir('added ' + filename)
               }
            })
         }
      })
      console.dir('updated blog: ' + JSON.stringify(doc))
      res.status(200).send()
   } else res.status(200).send()
})

app.listen(3001)
